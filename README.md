# registry-repository-delete-bash

A pipeline that generates a script (and optionally runs the script) to delete all GitLab container repositories from a list of selected GitLab projects.

_NOTE: The generated script should always be reviewed before running._

## Why this exists

As of the time of this tool/pipeline's inception; users with container repositories in their GitLab projects are unable to move/rename those projects. Efforts are being made to tackle this for projects on Gitlab.com https://gitlab.com/groups/gitlab-org/-/epics/9459 and eventually self-managed installs. 

For the time being however users will need to delete all registry repositories in projects they wish to transfer/move before a transfer is possible. 

This project simply leverages the [Container registry's APIs](https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository) to first fetch the registry repositories associated with a list of selected project and then generate a script  containing `curl` commands to delete each of the compiled registry repositories, one at a time.

Keep in mind the [DELETE API used in the curl commands is executed asynchronously and might take some time to complete the deletion in rails](https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository).

## Pre-requisites

1. A GitLab token with `read_api` on the projects to be deleted.
2. (Optional) A GitLab token with full access to the projects to be deleted.

## How it works

There are two jobs in the pipeline `generate` and `delete`. The pipeline's `generate` job generates a script with the necessary `curl` commands to run for to deleting all the repositories for the projects selected. The generated script is stored as a job artifact and can be downloaded and reviewed by a user (by browsing the job artifacts). Always inspect the script generated as a job artificat before running it. For projects that contain a large amount of repositories it is advisable to run the delete job locally to avoid timeouts on the CI pipeline or any other underlying inconsistencies.

The `delete` job is a manually triggered job that picks up the script created in the `generate` job and runs it. To run the delete job manually or otherwise you must first set the `GITLAB_DELETE_TOKEN` environment variable. The delete job only needs to be run once, but you can run the job multiple times if needed.

## How to use

To use, fork and run the pipeline against your projects of choice by re-setting the variables in the gitlab-ci.yml:

| Variable               | Description                                                                                                     | Default     |
| :--------------------- | :-------------------------------------------------------------------------------------------------------------: | :---------: |
| GITLAB_READ_TOKEN      |   A gitlab token with read access to gitlab APIs, this is used to generate the script a user will run locally   | |
| GITLAB_DELETE_TOKEN    |  (Optional) A gitlab token with full access to gitlab APIs, this is used to execute the deletion in the generated scrip         | |
| GITLAB_INSTANCE        |   Gitlab instance host                                                                                          | gitlab.com |
| PROJECT_PATHS          |   The space separated list of project paths from which all registry repositories should be deleted              | suleimiahmed/foo suleimiahmed/bar |


After running the generation job It is imperative to verify the generated script before running it locally or on a pipeline.
