#!/usr/bin/env bash

ME=$(basename "$0")
DEPENDS="curl jq"

die() {
  printf "\e[31;1m%s\e[0m\n" "$*" >&2
  exit 1
}

get_project_info() {
  local host=$1
  local project_name=$2
  local project_ns=${project_name//\//%2F}
  local url="https://${host}/api/v4/projects/${project_ns}/registry/repositories/"
  echo "Fetching registry repository IDs for project: $project_name" >&2
  local response
  if ! response=$(curl -s -w "%{http_code}" -H "PRIVATE-TOKEN:${GITLAB_READ_TOKEN}" -o /dev/null "$url"); then
    die "Curl command failed"
  fi
  local http_status="${response: -3}"
  if [[ $http_status -ne 200 && $http_status -ne 204 ]]; then
    die "Curl command failed with HTTP status code $http_status"
  fi
  local registry_response
  if ! registry_response=$(curl -s -H "PRIVATE-TOKEN:${GITLAB_READ_TOKEN}" "$url"); then
    die "Curl command failed"
  fi
  local registry_ids=()
  while IFS= read -r line; do
    registry_ids+=("$line")
  done < <(jq -r '.[].id' <<< "$registry_response")
  echo "${registry_ids[@]}"
}

delete_registry_repository() {
  local host=$1
  local project_name=$2
  local repository_id=$3
  local project_ns=${project_name//\//%2F}
  local url="https://${host}/api/v4/projects/${project_ns}/registry/repositories/${repository_id}"
  # echo "Deleting registry repository ID $repository_id for project $project_name"
  local echo_command="echo Deleting registry repository with id: \"$repository_id\""
  local curl_command="curl -w '\n' -X DELETE -s -H PRIVATE-TOKEN:\${GITLAB_DELETE_TOKEN} \"$url\""
  echo $echo_command | tee -a $TMPFILE >/dev/null
  echo $curl_command | tee -a $TMPFILE >/dev/null
: <<'END'
  local response
  if ! response=$(curl -X DELETE -s -w "%{http_code}" -H "PRIVATE-TOKEN:${GITLAB_DELETE_TOKEN}" "$url"); then
    die "Curl command failed"
  fi
  local http_status="${response: -3}"
  if [[ $http_status -eq 200 || $http_status -eq 202 ]]; then
    echo "repository ID $repository_id deleted successfully" >&2
  else
    die "Failed to delete repository: Curl: $response"
  fi
END
}

usage() {
  cat <<- EOM
		Usage: $ME <gitlab-server> <path-to-project1> <path-to-project2>
		Example: $ME gitlab.com johndoe/foo johndoe/bar
	EOM
  exit 1
}

main() {
  [[ ${GITLAB_READ_TOKEN} ]] || die "Please provide an access token via the environment variable GITLAB_READ_TOKEN"

  for CMD in $DEPENDS; do
    command -v $CMD >/dev/null 2>&1 || die "This script requires $CMD but it's not installed. Aborting."
  done

  [[ ${1} && ${2} ]] || usage

  local gitlab_host=$1
  shift

  mkdir -p ./${CI_PIPELINE_ID} && touch ./${CI_PIPELINE_ID}/$ME
  chmod 777 ./${CI_PIPELINE_ID}/$ME
  TMPFILE=./${CI_PIPELINE_ID}/$ME

  declare -A project_ids

  while [[ $# -gt 0 ]]; do
    local project_name=$1
    local registry_ids=($(get_project_info "$gitlab_host" "$project_name"))
    if [[ ${#registry_ids[@]} -gt 0 ]]; then
      for registry_id in "${registry_ids[@]}"; do
        delete_registry_repository "$gitlab_host" "$project_name" "$registry_id"
      done
    else
    echo "Skipping: no registry repositories found for $project_name"
    fi
    shift
  done

}

main "$@"
